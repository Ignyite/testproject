﻿using System;
using UnityEngine;


public class Inputmanager : MonoBehaviour
{
    public static float HorizontalAxis;

    private void Start()
    {
        HorizontalAxis = 0;
    }

    private void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");
        
    }
}
