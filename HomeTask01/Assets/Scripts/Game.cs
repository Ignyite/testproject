﻿using UnityEngine;
using Random = UnityEngine.Random;

public class Game : MonoBehaviour
{
    public static int gridWeight=10;
    public static int gridHeight = 12;
    [SerializeField] private GameObject[] m_figures;
    public  static Transform[,] grid=new Transform[gridWeight,gridHeight];
    void Start()
    {
        m_figures = GameObject.FindGameObjectsWithTag("figure");
        SpawnNextFigure();
    
    }
    
    // public void UpdateGrid(loweringFigure figure)
    // {
    //     for (int y = 0; y < gridHeight; ++y)
    //     {
    //         for (int x = 0; x < gridWeight; ++x)
    //         {
    //             if (grid[x, y] != null)
    //             {
    //                 if (grid[x, y] == figure.transform)
    //                 {
    //                     grid[x, y] = null;
    //                 }
    //             }
    //         }
    //     }
    //
    //     foreach (Transform cube in figure.transform )
    //     {
    //         Vector2 pos = Round(cube.position);
    //         if (pos.y < gridHeight)
    //         {
    //             grid[(int) pos.x, (int) pos.y] = cube;
    //         }
    //     }
    // }
    //
    // public Transform GetTransformAtGridPosition(Vector2 pos)
    // {
    //     if (pos.y > gridHeight - 1)
    //     {
    //         return null;
    //     }
    //     else
    //     {
    //         return grid[(int) pos.x, (int) pos.y];
    //     }
    // }
    
    public bool ChekIsInsideGrid(Vector2 pos)
    {
        return (int) pos.x > 0 && (int) pos.x <= gridWeight && (int) pos.y >=0;
    }

    public Vector2 Round(Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x),Mathf.Round(pos.y));
    }
    
    public void SpawnNextFigure()
    {
        print("spawn");
        GameObject nextFigure = (GameObject)Instantiate(Resources.Load(ChoiseFigure(),typeof(GameObject)),
            new Vector3(6.0f, 12f), Quaternion.identity);
        //GameObject nextFigure= GameObject.Instantiate(m_figures[Random.Range(0, m_figures.Length)],new Vector3(3f,11f), Quaternion.identity);
        if (nextFigure.GetComponent<loweringFigure>() == null)
        {
            print("компонента нет");
            nextFigure.AddComponent<loweringFigure>();
            
        }
        nextFigure.GetComponent<loweringFigure>().enabled = true;
            //nextFigure.SetActive(true);
    }

    private string ChoiseFigure()
    {
        int number = Random.Range(1, 4);
        string path = "Prefabs/GroupS";
        switch (number)
        {
            case 1: path="Prefabs/GroupS"; break;
            case 2: path="Prefabs/GroupT"; break;
            case 3: path="Prefabs/GroupG";
                break;
            default: break;
        }

        return path;
    }
}
