﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loweringFigure : MonoBehaviour
{
    
    [SerializeField] private float m_fallSpeed = 1f;

    
    private   float fall=0;
    //enum PlaceRespavn{first=1,second=2,third=3,fourth=4,fives=5,six=6,seven=7,eight=8,nine=9,ten=10};
   

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) )
        {
            gameObject.transform.position += new Vector3(-1, 0, 0);
            if (!ChekIsValidPosition())
            {
                gameObject.transform.position += new Vector3(1, 0, 0);
            }
            else
            {
               // FindObjectOfType<Game>().UpdateGrid(this);                
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) )
        {
            gameObject.transform.position += new Vector3(1, 0, 0);
            if (!ChekIsValidPosition())
            {
                gameObject.transform.position += new Vector3(-1, 0, 0);
            }
            else
            {
                //FindObjectOfType<Game>().UpdateGrid(this);                
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            gameObject.transform.Rotate(new Vector3(0, 0, 90));
            if (!ChekIsValidPosition())
            {
                gameObject.transform.Rotate(0,0,-90);
            }
            else
            {
                //FindObjectOfType<Game>().UpdateGrid(this);                
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Time.time-fall>=m_fallSpeed)
        {
            gameObject.transform.position+=new Vector3(0, -1, 0);
            if (!ChekIsValidPosition())
            {
                gameObject.transform.position += new Vector3(0, +1, 0);
                
                enabled = false;
                FindObjectOfType<Game>().SpawnNextFigure();
            }
            else
            {
               // FindObjectOfType<Game>().UpdateGrid(this);                
            }
            fall = Time.time;
        }
    }
    //private void OnCollisionEnter(Collision collision)
    //{
    //    print("nen");
    //    if (collision.gameObject.CompareTag("border"))
    //    {
    //        figureLowered = true;
    //    }
    //    LoweringNextFigure();
    //}
   
    // private void OnCollisionEnter2D(Collision2D collision)
    // {
    //    
    //     if (collision.gameObject.CompareTag("border"))
    //     {
    //         figureLowered = true;
    //     }
    //     
    //     LoweringNextFigure();
    // }
    //private void OnControllerColliderHit(ControllerColliderHit hit)
    //{
    //    print("nen");
    //    if (hit.gameObject.CompareTag("border"))
    //    {
    //        figureLowered = true;
    //    }
    //    LoweringNextFigure();
    //}

    bool ChekIsValidPosition()
    {
        foreach (Transform cube in transform )
        {
            Vector2 pos = FindObjectOfType<Game>().Round(cube.position);
            if (FindObjectOfType<Game>().ChekIsInsideGrid(pos) == false)
            {
                return false;
            }

            // if (FindObjectOfType<Game>().GetTransformAtGridPosition(pos) != null &&
            //     FindObjectOfType<Game>().GetTransformAtGridPosition(pos).parent != transform)
            // {
            //     return false;
            // }

        }

        return true;

    }
    
    // private void SpawnNextFigure()
    // {
    //
    //     
    //      GameObject nextFigure= GameObject.Instantiate(figures[Random.Range(0, figures.Length)],new Vector3(3f,10f), Quaternion.identity);
    //      nextFigure.SetActive(true);
    //     // nextFigure.AddComponent<loweringFigure>();
    //     // if(nextFigure.GetComponent<Rigidbody2D>()==null)
    //     //     nextFigure.AddComponent<Rigidbody2D>();
    //     // if(nextFigure.GetComponent<BoxCollider2D>()==null)
    //     //     nextFigure.AddComponent<BoxCollider2D>();
    //     // Destroy(nextFigure,5f);
    //     //GameObject.Destroy(gameObject);
    //     // nextFigure.transform.position = new Vector3(Mathf.Round(Random.Range(-4, 4)), 15f, 0f);
    // }
}
